import expect              from 'expect'

import longestCommonPrefix from './index.js'

describe('longestCommonPrefix', () => {
  it('should work', async () => {
    expect(longestCommonPrefix(['longest'])).toBe('longest')
    expect(longestCommonPrefix(['longest', 'long'])).toBe('long')
    expect(longestCommonPrefix(['longest', 'long', 'ok'])).toBe('long')
    expect(longestCommonPrefix(['longest', 'long', 'ok', 'whoaa'])).toBe(
      'whoaa',
    )
  })
})
