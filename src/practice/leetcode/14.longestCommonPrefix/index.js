export default function longestCommonPrefix(words) {
  const root = {}
  words.forEach(word => {
    root[word[0]] = root[word[0]] || {
      prefix: '',
      children: [],
    }

    const nodePrefix = lcp(root[word[0]].prefix, word)
    root[word[0]].prefix = nodePrefix.length > 0 ? nodePrefix : word
  })

  let longest = ''
  Object.entries(root).forEach(([key, { prefix }]) => {
    if (prefix.length > longest.length) {
      longest = prefix
    }
  })
  return longest
}

function lcp(a, b) {
  let prefix = ''
  for (let i = 0; i < a.length; i++) {
    if (a[i] !== b[i]) {
      break
    }
    prefix += a[i]
  }
  return prefix
}
