export default function solveSudoku(
  matrix,
  rows = {},
  columns = {},
  squares = {},
) {
  if (Object.keys(rows).length === 0) {
    for (let row = 0; row < matrix.length; row++) {
      for (let col = 0; col < matrix[row].length; col++) {
        rows[row] = rows[row] || {}
        columns[col] = columns[col] || {}
        squares[sqKey(row, col)] = squares[sqKey(row, col)] || {}
        const item = matrix[row][col]
        if (item !== '.') {
          rows[row][item] = true
          columns[col][item] = true
          squares[sqKey(row, col)][item] = true
        }
      }
    }
    printMatrix(matrix)
  }

  for (let row = 0; row < matrix.length; row++) {
    for (let col = 0; col < matrix[row].length; col++) {
      if (matrix[row][col] === '.') {
        const possibleNumbers = getPossibleNumbers(
          rows,
          columns,
          squares,
          row,
          col,
        )

        for (let i = 0; i < possibleNumbers.length; i++) {
          matrix[row][col] = possibleNumbers[i]
          rows[row][possibleNumbers[i]] = true
          columns[col][possibleNumbers[i]] = true
          squares[sqKey(row, col)][possibleNumbers[i]] = true

          const result = solveSudoku(matrix, rows, columns, squares)
          if (result) {
            return result
          }
          matrix[row][col] = '.'
          delete rows[row][possibleNumbers[i]]
          delete columns[col][possibleNumbers[i]]
          delete squares[sqKey(row, col)][possibleNumbers[i]]
        }
        return false
      }
    }
  }
  printMatrix(matrix)
  return matrix
}

function getPossibleNumbers(rows, columns, squares, i, j) {
  const existing = {
    ...rows[i],
    ...columns[j],
    ...squares[sqKey(i, j)],
  }
  return [1, 2, 3, 4, 5, 6, 7, 8, 9].filter(num => !existing[num])
}

function sqKey(row, col) {
  return `${Math.floor(row / 3)}, ${Math.floor(col / 3)}`
}

function printMatrix(matrix, i, j) {
  console.log(`-----------`)
  console.log(
    matrix
      .map((row, _i) => {
        row = row.map((r, _j) => {
          if (_i === i && _j === j) {
            return `[${r}]`
          }
          return r
        })
        return row.join('  ')
      })
      .join('\n'),
  )
  console.log(`-----------`)
}
