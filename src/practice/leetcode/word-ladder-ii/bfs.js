export default function findLadders(beginWord, endWord, wordList) {
  const adjacencyList = {}
  const queue = [beginWord]
  const visitedNodes = {}

  while (queue.length > 0) {
    const currentWord = queue.shift()
    visitedNodes[currentWord] = true
    adjacencyList[currentWord] = wordList
      .filter(word => !visitedNodes[word])
      .filter(word => wordsAreOneCharacterDiff(currentWord, word))
    adjacencyList[currentWord].forEach(word => queue.push(word))
  }
}

function wordsAreOneCharacterDiff(a, b) {
  let diff = 0
  for (let i = 0; i < a.length; i++) {
    if (a[i] !== b[i]) {
      diff++
    }
  }
  return diff === 1
}
