import expect      from 'expect'

import findLadders from './bfs.js'

describe('word ladder ii (bfs)', () => {
  it('works', async () => {
    const beginWord = 'abc'
    const endWord = 'acd'
    const wordList = ['abd', 'acc', 'acd']
    const results = findLadders(beginWord, endWord, wordList)

    results.every(ladder => {
      expect(ladder[0]).toBe(beginWord)
      expect(ladder[ladder.length - 1]).toBe(endWord)
      expect(ladder.length).toBe(5)
    })
  })

  // it('works', async () => {
  //   const beginWord = 'hit'
  //   const endWord = 'cog'
  //   const wordList = ['hot', 'dot', 'dog', 'lot', 'log', 'cog']
  //   const results = findLadders(beginWord, endWord, wordList)
  //
  //   results.every(ladder => {
  //     expect(ladder[0]).toBe(beginWord)
  //     expect(ladder[ladder.length - 1]).toBe(endWord)
  //     expect(ladder.length).toBe(5)
  //   })
  // })
  //
  // it('works', async () => {
  //   const beginWord = 'hit'
  //   const endWord = 'cog'
  //   const wordList = ['hot', 'dot', 'dog', 'lot', 'log', 'cog', 'cit', 'cot']
  //   const results = findLadders(beginWord, endWord, wordList)
  //
  //   results.every(ladder => {
  //     expect(ladder[0]).toBe(beginWord)
  //     expect(ladder[ladder.length - 1]).toBe(endWord)
  //     expect(ladder.length).toBe(5)
  //   })
  // })
})
