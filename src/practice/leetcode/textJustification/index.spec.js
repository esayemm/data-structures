import expect            from 'expect'

import textJustification from './index.js'

describe('textJustification', () => {
  it('should work', async () => {
    const words = [
      'This',
      'is',
      'an',
      'example',
      'of',
      'text',
      'justification.',
    ]
    const maxWidth = 16
    const result = textJustification(words, maxWidth)
    expect(result.join('\n')).toBe(
      ['This    is    an', 'example  of text', 'justification.  '].join('\n'),
    )
  })

  it('[""] expect [""]', async () => {
    const words = ['']
    const maxWidth = 16
    const result = textJustification(words, maxWidth)
    expect(result).toEqual([''])
  })
})
