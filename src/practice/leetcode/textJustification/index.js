export default function textJustification(words, maxWidth) {
  const lines = []

  for (let i = 0; i < words.length; i++) {
    if (!lines[lines.length - 1]) {
      lines.push([])
    }

    const word = words[i]
    if ((lines[lines.length - 1].join(' ') + ` ${word}`).length > maxWidth) {
      lines.push([word])
    } else {
      lines[lines.length - 1].push(word)
    }
  }

  return lines.map(line => justifyLine(line, maxWidth))
}

function justifyLine(line, maxWidth) {
  const paddingSpace = maxWidth - line.join('').length
  const spaceBetween = Math.floor(paddingSpace / (line.length - 1))
  let remainderSpaceBetween = paddingSpace % (line.length - 1)

  return line.reduce((constructedLine, word, i) => {
    if (word === '') {
      return word
    }
    constructedLine += word
    if (remainderSpaceBetween > 0) {
      constructedLine += ' '
      remainderSpaceBetween--
    }

    if (i !== line.length - 1) {
      constructedLine += ' '.repeat(spaceBetween)
    } else if (constructedLine.length !== maxWidth) {
      constructedLine += ' '.repeat(maxWidth - constructedLine.length)
    }

    return constructedLine
  }, '')
}
