export default function isMatch(a, b) {
  const aStack = a.split('').reverse()
  const bStack = b.split('').reverse()

  while (aStack.length > 0 || bStack.length > 0) {
    let bToken = bStack.pop()
    let aToken = aStack.pop()

    if (bToken === '?') {
      continue
    }

    if (bToken === '*') {
      bToken = bStack.pop()
      if (!bToken) {
        return true
      }

      if (aToken === bToken) {
        continue
      }

      aToken = aStack.pop()
      while (aToken && aToken !== bToken) {
        aToken = aStack.pop()
      }
    }

    if (aToken !== bToken) {
      return false
    }
  }

  return true
}
