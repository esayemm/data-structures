import expect           from 'expect'

import wildcardMatching from './index.js'

describe('wildcardMatching', () => {
  it('aa, a => false', async () => {
    expect(wildcardMatching('aa', 'a')).toBe(false)
  })

  it('aa, aa => true', async () => {
    expect(wildcardMatching('aa', 'aa')).toBe(true)
  })

  it('aaa, aa => false', async () => {
    expect(wildcardMatching('aaa', 'aa')).toBe(false)
  })

  it('aa, * => true', async () => {
    expect(wildcardMatching('aa', '*')).toBe(true)
  })

  it('aa, a* => true', async () => {
    expect(wildcardMatching('aa', 'a*')).toBe(true)
  })

  it('ab, ?* => true', async () => {
    expect(wildcardMatching('ab', '?*')).toBe(true)
  })

  it('aab, c*a*b => false', async () => {
    expect(wildcardMatching('aab', 'c*a*b')).toBe(false)
  })

  it('aab, *a*b => true', async () => {
    expect(wildcardMatching('aab', '*a*b')).toBe(true)
  })

  it('ab, *ab => true', async () => {
    expect(wildcardMatching('ab', '*ab')).toBe(true)
  })

  it('zacabz, *a?b* => false', async () => {
    expect(wildcardMatching('zacabz', '*a?b*')).toBe(false)
  })
})
