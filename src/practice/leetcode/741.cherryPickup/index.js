export default function cherryPickup(matrix) {
  const sum = traverse(matrix, 0, 0, matrix.length - 1, matrix[0].length - 1)
  return sum
}

function traverse(matrix, y, x, targetY, targetX) {
  if (matrix[y] === undefined) {
    return 0
  }
  if (matrix[y][x] === undefined) {
    return 0
  }

  const pickedUp = matrix[y][x] > -1 ? matrix[y][x] : 0
  matrix[y][x] = 0

  console.log(`-----------`)
  printHighlightedMatrix(matrix, y, x)
  console.log(`-----------`)

  return traverse(matrix, y + 1, x) + traverse(matrix, y, x + 1) + pickedUp
}

function printHighlightedMatrix(matrix, y, x) {
  console.log(
    matrix
      .map((row, i) =>
        row
          .map((item, j) => {
            return i === y && x === j ? `[${item}]` : item
          })
          .join(', '),
      )
      .join('\n'),
  )
}
