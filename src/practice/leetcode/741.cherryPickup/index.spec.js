import expect       from 'expect'

import cherryPickup from './index.js'

describe('cherryPickup', () => {
  const testCases = [
    {
      testCase: [[0, 1, -1], [1, 0, -1], [1, 1, 1]],
      expectedResults: 5,
    },
    {
      testCase: [[1, 1, -1], [1, -1, 1], [-1, 1, 1]],
      expectedResults: 0,
    },
  ]

  testCases.map(({ testCase, expectedResults }) => {
    it(`${testCase} => ${expectedResults}`, () => {
      expect(cherryPickup(testCase)).toBe(expectedResults)
    })
  })
})
