export default function mergeKSortedLists(lists) {
  const compare = (a = [], b = []) => {
    return a[0] < b[0]
  }

  const sorted = []
  const heap = []
  lists.forEach(list => insert(heap, list, compare))
  while (heap.length > 0) {
    const removed = remove(heap, compare)
    sorted.push(removed.shift())
    if (removed.length > 0) {
      insert(heap, removed, compare)
    }
  }
  return sorted
}

function printBinaryTreeArray(arr, cursor = 1, level = 0) {
  if (arr[cursor - 1] === undefined) {
    return
  }
  console.log(' '.repeat(level * 2), arr[cursor - 1])
  printBinaryTreeArray(arr, cursor * 2, level + 1)
  printBinaryTreeArray(arr, cursor * 2 + 1, level + 1)
}

function swap(arr, i, j) {
  const placeholder = arr[i]
  arr[i] = arr[j]
  arr[j] = placeholder
}

function insert(heap, value, compare) {
  heap.push(value)

  let cursor = heap.length
  while (cursor !== 0) {
    const parentIndex = Math.floor(cursor / 2)
    if (compare(heap[cursor], heap[parentIndex])) {
      swap(heap, cursor, parentIndex)
    } else {
      break
    }
    cursor = parentIndex
  }
}

function remove(heap, compare) {
  const removed = heap.shift()

  let cursor = 0
  while (cursor < heap.length) {
    let nextIndex
    if (cursor === 0) {
      nextIndex = !compare(heap[1], heap[2]) ? 1 : 2
    } else {
      nextIndex = !compare(heap[cursor * 2], heap[cursor * 2 + 1])
        ? cursor * 2
        : cursor * 2 + 1
    }

    if (nextIndex < heap.length && !compare(heap[cursor], heap[nextIndex])) {
      swap(heap, cursor, nextIndex)
      cursor = nextIndex
    } else {
      break
    }
  }

  return removed
}
