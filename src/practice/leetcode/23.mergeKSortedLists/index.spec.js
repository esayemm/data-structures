import expect            from 'expect'

import mergeKSortedLists from './index.js'

describe('mergeKSortedLists', () => {
  const testCases = [
    {
      testCase: [[1, 2, 3], [2, 3, 4], [3, 4, 5]],
      result: [1, 2, 2, 3, 3, 3, 4, 4, 5],
    },
  ]

  testCases.forEach(({ testCase, result }) => {
    it(`${testCase} => ${result}`, async () => {
      expect(mergeKSortedLists(testCase)).toEqual(result)
    })
  })
})
