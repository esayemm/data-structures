export default function longestValidParentheses(s) {
  let max = 0
  const stack = [-1]

  for (let i = 0; i < s.length; i++) {
    const token = s[i]

    if (token === '(') {
      stack.push(i)
    } else {
      stack.pop()
      const length = i - stack[stack.length - 1]
      if (length > max) {
        max = length
      }
    }
  }

  return max
}
