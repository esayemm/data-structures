import expect                  from 'expect'

import longestValidParentheses from './index.js'

describe('longestValidParentheses', () => {
  const testCases = [
    {
      testCase: `(()`,
      result: 2,
    },
    {
      testCase: `)()())`,
      result: 4,
    },
    {
      testCase: `()(()`,
      result: 2,
    },
  ]

  testCases.forEach(({ testCase, result }) => {
    it(`${testCase} => ${result}`, async () => {
      expect(longestValidParentheses(testCase)).toBe(result)
    })
  })
})
