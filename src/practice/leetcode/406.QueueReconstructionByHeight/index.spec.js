import expect from 'expect'

import queueReconstructionByHeight from './index.js'

describe('QueueReconstructionByHeight', () => {
  it('works', async () => {
    const testCase = [[7,0], [4,4], [7,1], [5,0], [6,1], [5,2]]
    const result = [[5,0], [7,0], [5,2], [6,1], [4,4], [7, 1]]
    expect(queueReconstructionByHeight(testCase)).toEqual(result)
  })
})
