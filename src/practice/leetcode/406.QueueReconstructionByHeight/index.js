export default function queueReconstructionByHeight(people) {
  people = [1, 2, 3, 4]
  return mergeSort(people, (a, b) => {
    console.log(a, b)
    return a < b ? -1 : 1
    // console.log(a[1], b[1])
    // return a[1] < b[1] ? 1 : -1
  })
}

function mergeSort(arr, compare) {
  if (arr.length === 1) {
    return arr
  }

  const left = arr.slice(0, arr.length / 2)
  const right = arr.slice(arr.length / 2)

  return merge(mergeSort(left, compare), mergeSort(right, compare), compare)
}

function merge(a, b, compare) {
  let aCursor = 0
  let bCursor = 0

  const sorted = []
  while (aCursor !== a.length && bCursor !== b.length) {
    const compareResult = compare(a[aCursor], b[bCursor]) > 0
    if (compareResult > 0) {
      sorted.push(a[aCursor])
      aCursor++
    } else if (compareResult < 0) {
      sorted.push(b[bCursor])
      bCursor++
    } else {
      sorted.push(a[aCursor])
      aCursor++
    }
  }
  return sorted.concat(a.slice(aCursor)).concat(b.slice(bCursor))
}
