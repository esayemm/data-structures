import expect                  from 'expect'

import medianOfTwoSortedArrays from './index.js'

describe('medianOfTwoSortedArrays', () => {
  it('works', async () => {
    expect(medianOfTwoSortedArrays([1, 2], [3, 4])).toBe(2.5)
  })

  it('works', async () => {
    expect(medianOfTwoSortedArrays([1, 3], [2])).toBe(2)
  })
})
