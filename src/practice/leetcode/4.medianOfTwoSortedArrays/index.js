export default function medianOfTwoSortedArrays(arr, arr2) {
  let cursorA = 0
  let cursorB = 0
  let sorted = []
  while (cursorA < arr.length && cursorB < arr2.length) {
    if (arr[cursorA] < arr2[cursorB]) {
      sorted.push(arr[cursorA])
      cursorA++
    } else {
      sorted.push(arr2[cursorB])
      cursorB++
    }
  }
  sorted = sorted.concat(arr.slice(cursorA)).concat(arr2.slice(cursorB))

  if (sorted.length % 2 === 0) {
    // even
    return (sorted[sorted.length / 2] + sorted[sorted.length / 2 - 1]) / 2
  } else {
    // odd
    return sorted[Math.floor(sorted.length / 2)]
  }
}
