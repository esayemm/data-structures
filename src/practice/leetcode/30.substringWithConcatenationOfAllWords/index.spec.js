import expect        from 'expect'

import findSubstring from './index.js'

describe('findSubstring', () => {
  const testCases = [
    {
      testCase: ['f', ['f']],
      result: [0],
    },
    {
      testCase: ['barfoothefoobarman', ['foo', 'bar']],
      result: [0, 9],
    },
  ]

  testCases.forEach(({ testCase, result }) => {
    it(`${testCase} => ${result}`, () => {
      expect(findSubstring(...testCase)).toEqual(result)
    })
  })
})
