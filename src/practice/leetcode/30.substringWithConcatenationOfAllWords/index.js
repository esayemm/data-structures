export default function findSubstring(str, words) {
  // empty case
  if (str.length === 0 || words.length === 0) {
    return []
  }
  const answer = []

  const wordLen = words[0].length
  const maximumMatchLen = words.join('').length
  for (let i = 0; i < wordLen; i++) {
    for (let j = i; j < str.length; j += wordLen) {
      const matches = words.reduce((map, word) => {
        map[word] = 0
        return map
      }, {})

      const currentWord = str.substring(j, j + wordLen)
      if (matches[currentWord] && matches[currentWord] === 0) {
        matches[currentWord]++
      } else {
      }
    }
  }
}
