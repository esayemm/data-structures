import expect   from 'expect'
import _        from 'lodash'

import LRUCache from './index.js'

describe('LRUCache', () => {
  it('1,2,3,4 inserts in max size cache should result in 2,3,4', async () => {
    const cache = new LRUCache({ maxSize: 3 })
    cache.put(1, 1)
    cache.put(2, 2)
    cache.put(3, 3)
    cache.put(4, 4)

    expect(cache.get(1)).toBe(-1)
    expect(cache.nodeRef[1]).not.toBeDefined()
    expect(cache.nodeRef[2]).toBeDefined()
    expect(cache.nodeRef[3]).toBeDefined()
    expect(cache.nodeRef[4]).toBeDefined()

    expect(_.isEqual(cache.queue.head.value, { key: 2, value: 2 })).toBe(true)
    expect(_.isEqual(cache.queue.head.next.value, { key: 3, value: 3 })).toBe(
      true,
    )
    expect(
      _.isEqual(cache.queue.head.next.next.value, { key: 4, value: 4 }),
    ).toBe(true)

    expect(cache.get(2)).toBe(2)
    expect(cache.get(3)).toBe(3)
    expect(cache.get(4)).toBe(4)

    cache.put(5, 5)
    expect(cache.get(2)).toBe(-1)
  })
})
