class Node {
  parent = undefined
  next = undefined
  prev = undefined
  value = undefined

  constructor(value, parent) {
    this.value = value
    this.parent = parent
  }

  // returns value
  remove() {
    if (this.next) {
      this.next.prev = this.prev
    }
    if (this.prev) {
      this.prev.next = this.next
    }

    if (this === this.parent.head) {
      this.parent.head = this.next
    }
    if (this.next === this) {
      this.parent.head = null
    }
    return this.value
  }
}

class Queue {
  head = undefined
  size = 0

  // returns Node
  enqueue(value) {
    if (!this.head) {
      this.head = new Node(value, this)
      this.head.next = this.head
      this.head.prev = this.head
      this.size++
      return this.head
    }

    const newNode = new Node(value, this)
    this.size++

    const tailNode = this.head.prev
    this.head.prev = newNode
    tailNode.next = newNode
    newNode.next = this.head
    newNode.prev = tailNode

    return newNode
  }

  // returns Node.value
  dequeue() {
    this.size--
    return this.head.remove()
  }
}

export default class LRUCache {
  maxSize = undefined
  queue = new Queue()
  nodeRef = {}

  constructor({ maxSize = Number.POSITIVE_INFINITY } = {}) {
    this.maxSize = maxSize
  }

  get = key => {
    const cacheHit = this.nodeRef[key]
    if (!cacheHit) {
      return -1
    }

    cacheHit.remove()
    const newNode = this.queue.enqueue({ key, value: cacheHit.value })
    this.nodeRef[key] = newNode
    return cacheHit.value.value
  }

  put = (key, value) => {
    const cacheHit = this.nodeRef[key]
    if (!cacheHit && this.queue.size + 1 > this.maxSize) {
      const dequeuedNode = this.queue.dequeue()
      delete this.nodeRef[dequeuedNode.key]
    }

    this.nodeRef[key] = this.queue.enqueue({ key, value })
  }
}
