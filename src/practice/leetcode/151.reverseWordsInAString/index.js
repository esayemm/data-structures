export default function reverseWords(str) {
  return str
    .split(' ')
    .filter(Boolean)
    .reverse()
    .join(' ')
}
