import expect       from 'expect'

import reverseWords from './index.js'

describe('reverseWordsInString', () => {
  it('a b c d => d c b a', async () => {
    expect(reverseWords('a b c d')).toBe('d c b a')
  })
})
