import expect      from 'expect'

import reversePair from './index'

describe('reversePair', () => {
  it('should work', async () => {
    expect(reversePair([1, 3, 2, 3, 1])).toEqual(2)
  })
})
