// https://swtch.com/~rsc/regexp/regexp1.html
export default function isMatch(str, pattern, i = 0, j = 0, cache = []) {
  cache[i] = cache[i] || []
  if (cache[i][j] !== undefined) {
    return cache[i][j]
  }

  let answer
  if (str.length === 0 && pattern.length === 0) {
    answer = true
  } else {
    const matchingChar =
      str[0] === pattern[0] || !!(str[0] && pattern[0] === '.')
    if (pattern[1] === '*') {
      answer =
        // empty
        isMatch(str, pattern.substring(2), i, j + 2, cache) ||
        (matchingChar &&
          // single
          (isMatch(
            str.substring(1),
            pattern.substring(2),
            i + 1,
            j + 2,
            cache,
          ) ||
            // repeat
            isMatch(str.substring(1), pattern, i + 1, j, cache)))
    } else {
      answer =
        matchingChar &&
        isMatch(str.substring(1), pattern.substring(1), i + 1, j + 1, cache)
    }
  }

  cache[i][j] = answer
  return answer
}
