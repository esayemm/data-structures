import expect                    from 'expect'

import regularExpressionMatching from './index.js'

describe('regularExpressionMatching', () => {
  const testCases = [
    {
      testCase: ['aa', 'a'],
      result: false,
    },
    {
      testCase: ['aaa', 'a.a'],
      result: true,
    },
    {
      testCase: ['aa', '.a'],
      result: true,
    },
    {
      testCase: ['aa', 'aa'],
      result: true,
    },
    {
      testCase: ['aaa', 'a'],
      result: false,
    },
    {
      testCase: ['aa', 'a*'],
      result: true,
    },
    {
      testCase: ['aa', '.*'],
      result: true,
    },
    {
      testCase: ['ab', '.*'],
      result: true,
    },
    {
      testCase: ['aab', 'c*a*b'],
      result: true,
    },
    {
      testCase: ['aaa', 'a*a'],
      result: true,
    },
    {
      testCase: ['a', 'b*a'],
      result: true,
    },
    {
      testCase: ['a', 'b*a'],
      result: true,
    },
    {
      testCase: ['ab', '.*c'],
      result: false,
    },
    // {
    //   testCase: ['abcd', 'd*'],
    //   result: false,
    // },
    // {
    //   testCase: ['aaa', 'ab*a'],
    //   result: false,
    // },
    // {
    //   testCase: ['a', 'ab*'],
    //   result: true,
    // },
    // {
    //   testCase: ['aaaaaaaaaaaaab', 'a*a*a*a*a*a*a*a*a*a*c'],
    //   result: false,
    // },
    // {
    //   testCase: ['', 'a*a*'],
    //   result: true,
    // },
  ]

  testCases.forEach(({ testCase, result }) => {
    it(`${testCase} => ${result}`, async () => {
      expect(regularExpressionMatching(...testCase)).toBe(result)
    })
  })
})
