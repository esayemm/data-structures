import expect    from 'expect'

import minMaxSum from './index.js'

describe('minMaxSum', () => {
  it('should work', async () => {
    expect(minMaxSum([1, 2, 3, 4, 5])).toEqual([10, 14])
  })
})
