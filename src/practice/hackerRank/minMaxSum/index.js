export default function minMaxSum(numbers) {
  let min = Number.POSITIVE_INFINITY
  let max = Number.NEGATIVE_INFINITY

  const sum = numbers.reduce((total, item) => total + item, 0)
  for (let i = 0; i < numbers.length; i++) {
    const cursorSum = sum - numbers[i]
    if (cursorSum < min) {
      min = cursorSum
    }
    if (cursorSum > max) {
      max = cursorSum
    }
  }

  return [min, max]
}
