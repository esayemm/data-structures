// width and height must be even numbers
export default function matrixLayerRotation(width, height, rotations, matrix) {
  let cursor = { x: 0, y: 0 }
  let direction = { x: 0, y: 1 }
  let level = 0

  while (level < width / 2) {
    const queue = []
    const total = (width - level * 2) * 2 + (height - level * 2) * 2 - 4
    const modRotations = rotations % total
    let rotationCursor = 0

    do {
      if (rotationCursor < total) {
        queue.push(matrix[cursor.y][cursor.x])
      }
      if (rotationCursor >= modRotations) {
        matrix[cursor.y][cursor.x] = queue.shift()
      }

      if (cursor.x + direction.x < level) {
        direction = { x: 0, y: 1 }
      } else if (cursor.y + direction.y > height - 1 - level) {
        direction = { x: 1, y: 0 }
      } else if (cursor.x + direction.x > width - 1 - level) {
        direction = { x: 0, y: -1 }
      } else if (cursor.y + direction.y < level) {
        direction = { x: -1, y: 0 }
      }
      cursor = { x: cursor.x + direction.x, y: cursor.y + direction.y }
      rotationCursor++
    } while (queue.length > 0)

    level++
    cursor = { x: 0 + level, y: 0 + level }
    direction = { x: 0, y: 1 }
  }
  return matrix
}
