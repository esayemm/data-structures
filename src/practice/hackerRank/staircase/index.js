export default function staircase(n) {
  const lines = []
  for (let i = 1; i < n + 1; i++) {
    lines.push(` `.repeat(n - i) + `#`.repeat(i))
  }
  return lines.join('\n')
}
