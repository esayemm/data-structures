import expect    from 'expect'

import staircase from './index.js'

describe('staircase', () => {
  it('should work', async () => {
    expect(staircase(6)).toBe(
      [`     #`, `    ##`, `   ###`, `  ####`, ` #####`, `######`].join('\n'),
    )
  })
})
