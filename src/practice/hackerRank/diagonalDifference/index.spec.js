import expect             from 'expect'

import diagonalDifference from './index.js'

describe('diagonalDifference', () => {
  it('should work', async () => {
    expect(diagonalDifference([[11, 2, 4], [4, 5, 6], [10, 8, -12]])).toBe(15)
  })
})
