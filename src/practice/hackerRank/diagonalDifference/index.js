// matrix has to be N
export default function diagonalDifference(matrix) {
  let cursor = { x: 0, y: 0 }
  const width = matrix.length

  let sum1 = 0
  while (cursor.x < width) {
    sum1 += matrix[cursor.y][cursor.x]
    cursor.x++
    cursor.y++
  }

  let sum2 = 0
  cursor = { x: 0, y: width - 1 }
  while (cursor.x < width) {
    sum2 += matrix[cursor.y][cursor.x]
    cursor.x++
    cursor.y--
  }

  return Math.abs(sum2 - sum1)
}
