import expect    from 'expect'

import mergeSort from './index'

describe('mergeSort', () => {
  it('sorts', async () => {
    const testCase = [5, 3, 4, 1, 2]
    expect(mergeSort(testCase.slice())).toEqual(testCase.sort())
  })

  it('[1]', async () => {
    const testCase = [1]
    expect(mergeSort(testCase.slice())).toEqual(testCase.sort())
  })

  it('[]', async () => {
    const testCase = []
    expect(mergeSort(testCase.slice())).toEqual(testCase.sort())
  })
})
