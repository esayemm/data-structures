class Node {
  next = undefined
  value = undefined

  constructor(value) {
    this.value = value
  }
}

export default class Stack {
  tail = undefined
  size = 0

  push = value => {
    if (!this.tail) {
      this.tail = new Node(value)
      this.size++
      return
    }

    const tailNode = this.tail
    this.tail = new Node(value)
    this.size++
    this.tail.next = tailNode
  }

  pop = () => {
    if (!this.tail) {
      return
    }

    const tailNode = this.tail
    this.tail = tailNode.next
    this.size--
    return tailNode.value
  }

  peek = () => {
    if (!this.tail) {
      return
    }

    return this.tail.value
  }
}
