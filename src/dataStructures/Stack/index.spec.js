import expect from 'expect'

import Stack  from './index.js'

describe('Stack', () => {
  describe('push()', () => {
    it('should work', async () => {
      const stack = new Stack()
      stack.push(1)

      expect(stack.size).toBe(1)
      expect(stack.tail.value).toBe(1)
      expect(stack.tail.next).toBe(undefined)

      stack.push(2)
      expect(stack.size).toBe(2)
      expect(stack.tail.value).toBe(2)
      expect(stack.tail.next.value).toBe(1)
      expect(stack.tail.next.next).toBe(undefined)
    })
  })

  describe('pop()', () => {
    it('should work', async () => {
      const stack = new Stack()
      stack.push(1)
      stack.push(2)
      stack.push(3)

      expect(stack.pop()).toBe(3)
      expect(stack.size).toBe(2)
      expect(stack.pop()).toBe(2)
      expect(stack.size).toBe(1)
      expect(stack.pop()).toBe(1)
      expect(stack.size).toBe(0)
      expect(stack.tail).toBe(undefined)
    })
  })

  describe('peek()', () => {
    it('should work', async () => {
      const stack = new Stack()
      stack.push(1)
      stack.push(2)
      stack.push(3)

      expect(stack.peek()).toBe(3)
    })
  })
})
