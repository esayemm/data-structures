import expect                                from 'expect'

import RadixTree, { getLongestCommonPrefix } from './index.js'

describe('RadixTree', () => {
  describe('insert()', () => {
    it('insert abc', async () => {
      const radixTree = new RadixTree()
      radixTree.insert('abc', true)
      expect(radixTree.root.children.a.prefix).toBe('abc')
      expect(radixTree.root.children.a.value).toBe(true)
    })

    it('insert abc then insert bcd', async () => {
      const radixTree = new RadixTree()
      radixTree.insert('abc', true)
      radixTree.insert('bcd', true)
      expect(radixTree.root.children.a.prefix).toBe('abc')
      expect(radixTree.root.children.a.value).toBe(true)
      expect(radixTree.root.children.b.prefix).toBe('bcd')
      expect(radixTree.root.children.b.value).toBe(true)
    })

    it('insert abc then insert abc should update', async () => {
      const radixTree = new RadixTree()
      radixTree.insert('abc', true)
      expect(radixTree.root.children.a.prefix).toBe('abc')
      expect(radixTree.root.children.a.value).toBe(true)
      radixTree.insert('abc', false)
      expect(radixTree.root.children.a.prefix).toBe('abc')
      expect(radixTree.root.children.a.value).toBe(false)
    })

    it('insert abc then insert abce then insert abcd', async () => {
      const radixTree = new RadixTree()
      radixTree.insert('abc', true)
      radixTree.insert('abce', true)
      radixTree.insert('abcd', true)
      expect(radixTree.root.children.a.prefix).toBe('abc')
      expect(radixTree.root.children.a.value).toBe(true)
      expect(radixTree.root.children.a.children.e.prefix).toBe('e')
      expect(radixTree.root.children.a.children.e.value).toBe(true)
      expect(radixTree.root.children.a.children.d.prefix).toBe('d')
      expect(radixTree.root.children.a.children.d.value).toBe(true)
    })

    it('insert abc then insert ab then insert a', async () => {
      const radixTree = new RadixTree()
      radixTree.insert('abc', true)
      radixTree.insert('ab', true)
      radixTree.insert('a', true)
      expect(radixTree.root.children.a.prefix).toBe('a')
      expect(radixTree.root.children.a.value).toBe(true)
      expect(radixTree.root.children.a.children.b.prefix).toBe('b')
      expect(radixTree.root.children.a.children.b.value).toBe(true)
      expect(radixTree.root.children.a.children.b.children.c.prefix).toBe('c')
      expect(radixTree.root.children.a.children.b.children.c.value).toBe(true)
    })

    it('insert ab then a', async () => {
      const radixTree = new RadixTree()
      radixTree.insert('ab', true)
      radixTree.insert('a', true)
      expect(
        Object.keys(radixTree.root.children.a.children.b.children).length,
      ).toBe(0)
    })

    it('insert abc,ab,a', async () => {
      const radixTree = new RadixTree()
      radixTree.insert('abc', true)
      expect(Object.keys(radixTree.root.children.a.children).length).toBe(0)
      radixTree.insert('ab', true)
      expect(Object.keys(radixTree.root.children.a.children).length).toBe(1)
      expect(radixTree.root.children.a.children.c.value).toBe(true)
      radixTree.insert('a', true)
      expect(Object.keys(radixTree.root.children.a.children).length).toBe(1)
      expect(
        Object.keys(radixTree.root.children.a.children.b.children).length,
      ).toBe(1)
    })

    it('insert abc,abe,ab', async () => {
      const radixTree = new RadixTree()
      radixTree.insert('abc', true)
      radixTree.insert('abe', true)
      radixTree.insert('ab', true)
      expect(Object.keys(radixTree.root.children.a.children).length).toBe(2)
    })
  })

  describe('remove()', () => {
    it('remove invalid key should return false', async () => {
      const radixTree = new RadixTree()
      expect(radixTree.remove('a')).toBe(false)
    })

    it('remove a from radix tree with just a', async () => {
      const radixTree = new RadixTree()
      radixTree.insert('a', true)
      radixTree.remove('a')
      expect(radixTree.root.children.a).toBe(undefined)
    })

    it('remove a from radix tree with just ab', async () => {
      const radixTree = new RadixTree()
      radixTree.insert('ab', true)
      expect(radixTree.remove('a')).toBe(false)
      expect(radixTree.root.children.a.prefix).toBe('ab')
    })

    it('remove a from radix tree with ab and a', async () => {
      const radixTree = new RadixTree()
      radixTree.insert('ab', true)
      radixTree.insert('a', true)
      expect(radixTree.root.children.a.prefix).toBe('a')
      expect(radixTree.remove('a')).toBe(true)
      expect(radixTree.root.children.a.prefix).toBe('ab')
      expect(radixTree.root.children.a.value).toBe(true)
      expect(Object.keys(radixTree.root.children.a.children).length).toBe(0)
    })

    it('remove a from a,abc', async () => {
      const radixTree = new RadixTree()
      radixTree.insert('abc', true)
      radixTree.insert('ab', true)
      radixTree.insert('a', true)
      radixTree.remove('a')
      expect(radixTree.root.children.a.prefix).toBe('ab')
    })
  })

  describe('lookup()', () => {
    it('empty tree return undefined', async () => {
      const radixTree = new RadixTree()
      expect(radixTree.lookup()).toBe(undefined)
    })

    it('abc lookup a should return undefined', async () => {
      const radixTree = new RadixTree()
      radixTree.insert('abc', true)
      expect(radixTree.lookup('a')).toBe(undefined)
    })

    it('abc lookup abc should return value', async () => {
      const radixTree = new RadixTree()
      radixTree.insert('abc', true)
      expect(radixTree.lookup('abc')).toBe(true)
    })

    it('filled tree lookups', async () => {
      const radixTree = new RadixTree()
      radixTree.insert('abc', true)
      radixTree.insert('abce', true)
      radixTree.insert('bc', true)
      radixTree.insert('apl', true)
      expect(radixTree.lookup('abc')).toBe(true)
      expect(radixTree.lookup('abce')).toBe(true)
      expect(radixTree.lookup('bc')).toBe(true)
      expect(radixTree.lookup('apl')).toBe(true)
      expect(radixTree.lookup('ab')).toBe(undefined)
      expect(radixTree.lookup('a')).toBe(undefined)
      expect(radixTree.lookup('b')).toBe(undefined)
    })
  })

  describe('getSize()', () => {
    it('duplicate insert should not change size', async () => {
      const radixTree = new RadixTree()
      radixTree.insert('abc', true)
      expect(radixTree.getSize()).toBe(1)
      radixTree.insert('abc', true)
      expect(radixTree.getSize()).toBe(1)
    })

    it('remove nonexistent should not change size', async () => {
      const radixTree = new RadixTree()
      radixTree.insert('abc', true)
      expect(radixTree.getSize()).toBe(1)
      radixTree.remove('ab')
      expect(radixTree.getSize()).toBe(1)
    })

    it('insert remove should change size', async () => {
      const radixTree = new RadixTree()
      radixTree.insert('abc', true)
      expect(radixTree.getSize()).toBe(1)
      radixTree.insert('ab', true)
      expect(radixTree.getSize()).toBe(2)
      radixTree.insert('abce', true)
      expect(radixTree.getSize()).toBe(3)
      radixTree.insert('abcd', true)
      expect(radixTree.getSize()).toBe(4)
      radixTree.insert('abcz', true)
      expect(radixTree.getSize()).toBe(5)
      radixTree.remove('abce')
      expect(radixTree.getSize()).toBe(4)
      radixTree.remove('abc')
      expect(radixTree.getSize()).toBe(3)
    })
  })

  describe('keys()', () => {
    it('should return keys in RadixTree', async () => {
      const testCases = ['abc', 'ab', 'abce', 'abcx', 'abcy']
      const radixTree = new RadixTree()
      testCases.forEach(key => radixTree.insert(key, true))
      const results = radixTree.keys()
      expect(results.length).toBe(testCases.length)
      expect(results.every(key => testCases.includes(key))).toBe(true)
    })
  })
})

describe('getLongestCommonPrefix', () => {
  it('abc ab should return ab', async () => {
    expect(getLongestCommonPrefix('abc', 'ab')).toBe('ab')
  })

  it('abce abcd should return abc', async () => {
    expect(getLongestCommonPrefix('abce', 'abcd')).toBe('abc')
  })

  it('abc and null should return nothing', async () => {
    expect(getLongestCommonPrefix('abc')).toBe('')
  })
})
