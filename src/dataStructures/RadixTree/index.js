class RadixNode {
  prefix = ''
  children = {}
  value = undefined

  constructor(opts = {}) {
    Object.entries(opts).forEach(([key, value]) => {
      this[key] = value
    })
  }
}

export function getLongestCommonPrefix(a = '', b = '') {
  let prefix = ''
  for (let i = 0; i < a.length; i++) {
    if (b[i] && a[i] === b[i]) {
      prefix += a[i]
    } else {
      break
    }
  }
  return prefix
}

export default class RadixTree {
  root = new RadixNode()
  size = 0

  insert(key, value, node = this.root) {
    const nextNode = node.children[key[0]]
    if (!nextNode) {
      node.children[key[0]] = new RadixNode({
        prefix: key,
        value,
      })
      this.size++
      return
    }

    if (key === nextNode.prefix) {
      nextNode.value = value
      return
    }

    const longestCommonPrefix = getLongestCommonPrefix(key, nextNode.prefix)
    const remainderKey = key.substring(longestCommonPrefix.length)
    const remainderExistingPrefix = nextNode.prefix.substring(
      longestCommonPrefix.length,
    )

    if (remainderExistingPrefix.length > 0) {
      node.children[key[0]] = new RadixNode({
        prefix: longestCommonPrefix,
        children: {
          [remainderExistingPrefix[0]]: {
            ...nextNode,
            prefix: remainderExistingPrefix,
          },
        },
      })
      if (!remainderKey.length) {
        node.children[key[0]].value = value
        this.size++
      }
    }
    if (remainderKey.length > 0) {
      return this.insert(remainderKey, value, node.children[key[0]])
    }
  }

  remove(key = '', node = this.root) {
    const nextNode = node.children[key[0]]
    if (!nextNode) {
      return false
    }
    if (key === nextNode.prefix) {
      const ret = nextNode.value
      nextNode.value = undefined
      this.size--

      // merge parent with child
      const nextNodeChildrenKeys = Object.keys(nextNode.children)
      if (nextNodeChildrenKeys.length === 1) {
        nextNode.prefix =
          nextNode.prefix + nextNode.children[nextNodeChildrenKeys[0]].prefix
        nextNode.value = nextNode.children[nextNodeChildrenKeys[0]].value
        nextNode.children = nextNode.children[nextNodeChildrenKeys[0]].children
      } else if (nextNodeChildrenKeys.length === 0) {
        delete node.children[key[0]]
      } else {
        nextNode.value = undefined
      }
      return ret
    }

    const longestCommonPrefix = getLongestCommonPrefix(key, nextNode.prefix)
    const remainderKey = key.substring(longestCommonPrefix.length)
    return this.remove(remainderKey, nextNode)
  }

  lookup(key = '', node = this.root) {
    if (key.length === 0) {
      return node.value
    }

    const nextNode = node.children[key[0]]
    if (!nextNode) {
      return
    }

    const longestCommonPrefix = getLongestCommonPrefix(key, nextNode.prefix)
    const remainderKey = key.substring(longestCommonPrefix.length)
    const remainderExistingPrefix = nextNode.prefix.substring(
      longestCommonPrefix.length,
    )
    if (remainderExistingPrefix.length > 0) {
      return
    }
    return this.lookup(remainderKey, nextNode)
  }

  getSize() {
    return this.size
  }

  clear() {
    this.root.children = {}
    this.size = 0
  }

  keys(node = this.root) {
    return [].concat(
      ...Object.entries(node.children).map(([_, childNode]) => {
        if (Object.keys(childNode.children).length === 0) {
          return [childNode.prefix]
        } else {
          return [childNode.value !== undefined && childNode.prefix]
            .filter(Boolean)
            .concat(
              ...this.keys(childNode).map(suffix => childNode.prefix + suffix),
            )
        }
      }),
    )
  }
}
