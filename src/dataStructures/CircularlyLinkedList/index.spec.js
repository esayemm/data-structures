import expect               from 'expect'

import CircularlyLinkedList from './index.js'

describe('CircularlyLinkedList', () => {
  describe('append()', () => {
    it('append 1', async () => {
      const list = new CircularlyLinkedList()
      list.append(1)
      expect(list.head.value).toBe(1)
      expect(list.size).toBe(1)
      expect(list.head.next).toBe(list.head)
      expect(list.head.prev).toBe(list.head)
    })

    it('append 2', async () => {
      const list = new CircularlyLinkedList()
      list.append(1)
      list.append(2)
      expect(list.head.value).toBe(1)
      expect(list.size).toBe(2)
      expect(list.head.next.value).toBe(2)
      expect(list.head.next.next).toBe(list.head)
      expect(list.head.prev).toBe(list.head.next)
    })
  })

  describe('insertAfter()', () => {
    it('1,2,3 insertAfter on node 2', async () => {
      const list = new CircularlyLinkedList()
      list.append(1)
      const node2 = list.append(2)
      list.append(3)

      list.insertAfter(node2, 10)

      expect(list.size).toBe(4)
      expect(list.head.value).toBe(1)
      expect(list.head.next.value).toBe(2)
      expect(list.head.next.next.value).toBe(10)
      expect(list.head.next.next.next.value).toBe(3)
    })
  })
})
