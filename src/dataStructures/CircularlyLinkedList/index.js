class ListNode {
  parent = undefined
  next = undefined
  prev = undefined
  value = undefined

  constructor(value, parent) {
    this.value = value
    this.parent = parent
  }

  /**
   * remove will remove this node from list
   */
  remove() {
    if (this.next) {
      this.next.prev = this.prev
    }
    if (this.prev) {
      this.prev.next = this.next
    }

    if (this === this.parent.head) {
      this.parent.head = this.next
    }
    if (this.next === this) {
      this.parent.head = null
    }
    return this.value
  }
}

export default class CircularlyLinkedList {
  head = undefined
  size = 0

  /**
   * insertAfter inserts value after given node
   */
  insertAfter(node, value) {
    const newNode = new ListNode(value)
    this.size++

    newNode.prev = node
    newNode.next = node.next

    node.next.prev = newNode
    node.next = newNode
  }

  /**
   * append will add
   */
  append(value) {
    if (!this.head) {
      this.head = new ListNode(value, this)
      this.head.next = this.head
      this.head.prev = this.head
      this.size++
      return this.head
    }

    const newNode = new ListNode(value, this)
    this.size++

    const tailNode = this.head.prev
    this.head.prev = newNode
    tailNode.next = newNode
    newNode.next = this.head
    newNode.prev = tailNode

    return newNode
  }

  /**
   * shift will return value of head node
   */
  shift() {
    this.size--
    return this.head.remove()
  }
}
