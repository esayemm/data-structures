import expect     from 'expect'

import LinkedList from './index.js'

describe('LinkedList', () => {
  describe('append()', () => {
    it('one item', async () => {
      const list = new LinkedList()
      list.append(1)
      expect(list.head.value).toBe(1)
    })

    it('multiple items', async () => {
      const list = new LinkedList()
      list.append(1)
      list.append(2)
      list.append(3)
      expect(list.head.value).toBe(1)
      expect(list.head.next.value).toBe(2)
      expect(list.head.next.next.value).toBe(3)
    })
  })

  describe('prepend()', () => {
    it('one item', async () => {
      const list = new LinkedList()
      list.prepend(1)
      expect(list.head.value).toBe(1)
    })

    it('multiple items', async () => {
      const list = new LinkedList()
      list.prepend(1)
      list.prepend(2)
      list.prepend(3)
      expect(list.head.value).toBe(3)
      expect(list.head.next.value).toBe(2)
      expect(list.head.next.next.value).toBe(1)
    })
  })

  describe('remove()', () => {
    it('one item from empty list', async () => {
      const list = new LinkedList()
      list.remove(1)
    })

    it('one item from [1]', async () => {
      const list = new LinkedList()
      list.append(1)
      list.remove(1)
      expect(list.head).toBe(undefined)
    })

    it('remove 2 from [1,2]', async () => {
      const list = new LinkedList()
      list.append(1)
      list.append(2)
      list.remove(2)
      expect(list.head.value).toBe(1)
      expect(list.head.next).toBe(undefined)
    })
    it('remove 1 from [1,2]', async () => {
      const list = new LinkedList()
      list.append(1)
      list.append(2)
      list.remove(1)
      expect(list.head.value).toBe(2)
      expect(list.head.next).toBe(undefined)
    })
  })
})
