class ListNode {
  value = undefined
  next = undefined

  constructor(value) {
    this.value = value
  }
}

export default class LinkedList {
  head = undefined

  append(value) {
    if (!this.head) {
      this.head = new ListNode(value)
      return
    }
    let cursor = this.head
    while (cursor.next) {
      cursor = cursor.next
    }
    cursor.next = new ListNode(value)
  }

  prepend(value) {
    if (!this.head) {
      this.head = new ListNode(value)
      return
    }

    const previousHead = this.head
    this.head = new ListNode(value)
    this.head.next = previousHead
  }

  remove(value) {
    if (!this.head) {
      return
    }

    if (this.head.value === value) {
      this.head = this.head.next
      return
    }

    let cursor = this.head
    while (cursor.next) {
      if (cursor.next.value === value) {
        cursor.next = cursor.next.next
        return
      }
      cursor = cursor.next
    }
  }
}
