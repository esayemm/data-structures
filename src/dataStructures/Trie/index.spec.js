import expect from 'expect'

import Trie   from './index.js'

describe('Trie', () => {
  describe('insert()', () => {
    it('insert single character', async () => {
      const trie = new Trie()
      trie.insert('a', true)
      expect(trie.root.children.a.value).toBe(true)
    })

    it('insert multiple character string', async () => {
      const trie = new Trie()
      trie.insert('abc', true)
      expect(trie.root.children.a.children.b.children.c.value).toBe(true)
    })

    it('insert multiple strings with same prefix', async () => {
      const trie = new Trie()
      trie.insert('abc', true)
      trie.insert('abd', true)
      expect(trie.root.children.a.children.b.children.c.value).toBe(true)
      expect(trie.root.children.a.children.b.children.d.value).toBe(true)
    })

    it('insert multiple strings without same prefix', async () => {
      const trie = new Trie()
      trie.insert('abc', true)
      trie.insert('bcd', true)
      expect(trie.root.children.a.children.b.children.c.value).toBe(true)
      expect(trie.root.children.b.children.c.children.d.value).toBe(true)
    })

    it('insert abc then insert ab', async () => {
      const trie = new Trie()
      trie.insert('abc', true)
      trie.insert('ab', true)
      expect(trie.root.children.a.children.b.children.c.value).toBe(true)
      expect(trie.root.children.a.children.b.value).toBe(true)
    })

    it('insert ab then insert abc', async () => {
      const trie = new Trie()
      trie.insert('ab', true)
      trie.insert('abc', true)
      expect(trie.root.children.a.children.b.value).toBe(true)
      expect(trie.root.children.a.children.b.children.c.value).toBe(true)
    })

    it('insert abc, abe, ab', async () => {
      const trie = new Trie()
      trie.insert('abc', true)
      trie.insert('abe', true)
      trie.insert('ab', true)
      expect(trie.root.children.a.children.b.value).toBe(true)
      expect(trie.root.children.a.children.b.children.c.value).toBe(true)
      expect(trie.root.children.a.children.b.children.e.value).toBe(true)
    })

    it('insert over existing key should update', async () => {
      const trie = new Trie()
      trie.insert('ab', true)
      expect(trie.root.children.a.children.b.value).toBe(true)
      trie.insert('ab', false)
      expect(trie.root.children.a.children.b.value).toBe(false)
    })
  })

  describe('remove()', () => {
    it('abc remove abc should be empty', async () => {
      const trie = new Trie()
      trie.insert('abc', true)
      trie.remove('abc')
      expect(Object.keys(trie.root.children).length).toBe(0)
    })

    it('abcd remove abc should still have d leaf node', async () => {
      const trie = new Trie()
      trie.insert('abcd', true)
      trie.remove('abc')
      expect(trie.root.children.a.children.b.children.c.children.d.value).toBe(
        true,
      )
    })

    it('abc abe ab remove ab should not prune the leaf nodes', async () => {
      const trie = new Trie()
      trie.insert('abc', true)
      trie.insert('abe', true)
      trie.insert('ab', true)
      expect(trie.root.children.a.children.b.value).toBe(true)
      trie.remove('ab')
      expect(trie.root.children.a.children.b.value).toBe(undefined)
      expect(trie.root.children.a.children.b.children.c.value).toBe(true)
      expect(trie.root.children.a.children.b.children.e.value).toBe(true)
    })
  })

  describe('lookup()', () => {
    it('abc should lookup abc', async () => {
      const trie = new Trie()
      trie.insert('abc', true)
      expect(trie.lookup('abc')).toBe(true)
    })

    it('look up key that is not in trie should return undefined', async () => {
      const trie = new Trie()
      expect(trie.lookup('abc')).toBe(undefined)
    })
  })

  describe('getSize()', () => {
    it('insert remove should change size', async () => {
      const trie = new Trie()
      trie.insert('abc', true)
      trie.insert('abe', true)
      expect(trie.getSize()).toBe(2)
      trie.remove('abc')
      trie.remove('ab')
      expect(trie.getSize()).toBe(1)
      trie.insert('foo', true)
      expect(trie.getSize()).toBe(2)
      trie.remove('foo')
      trie.remove('foo')
      expect(trie.getSize()).toBe(1)
      trie.remove('abe')
      expect(trie.getSize()).toBe(0)
    })
  })

  describe('keys()', () => {
    it('trie containing abc ab abe should return array of strings', async () => {
      const trie = new Trie()
      trie.insert('abc', true)
      trie.insert('ab', true)
      trie.insert('abe', true)

      const results = trie.keys()
      expect(results.includes('abc')).toBe(true)
      expect(results.includes('ab')).toBe(true)
      expect(results.includes('abe')).toBe(true)
    })
  })
})
