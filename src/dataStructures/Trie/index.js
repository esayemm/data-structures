class TrieNode {
  children = {}
  value = undefined
}

export default class Trie {
  root = new TrieNode()
  size = 0

  insert(key, value, node = this.root) {
    const cursorChar = key[0]
    const remainder = key.substring(1)
    if (!node.children[cursorChar]) {
      node.children[cursorChar] = new TrieNode()
    }
    if (remainder.length === 0) {
      node.children[cursorChar].value = value
      this.size++
      return
    }
    return this.insert(remainder, value, node.children[cursorChar])
  }

  remove(key, node = this.root) {
    const cursorChar = key[0]
    const remainder = key.substring(1)
    if (!node.children[cursorChar]) {
      return
    }
    if (
      remainder.length === 0 &&
      node.children[cursorChar].value !== undefined
    ) {
      node.children[cursorChar].value = undefined
      this.size--
    }
    this.remove(remainder, node.children[cursorChar])
    if (Object.keys(node.children[cursorChar].children).length === 0) {
      delete node.children[cursorChar]
    }
  }

  lookup(key, node = this.root) {
    const cursorChar = key[0]
    const remainder = key.substring(1)
    if (!node.children[cursorChar]) {
      return
    }
    if (remainder.length === 0) {
      return node.children[cursorChar].value
    }
    return this.lookup(remainder, node.children[cursorChar])
  }

  getSize() {
    return this.size
  }

  clear() {
    this.root.children = {}
    this.size = 0
  }

  keys(node = this.root) {
    return [].concat(
      ...Object.entries(node.children).map(([key, childNode]) => {
        if (Object.keys(childNode.children).length === 0) {
          return [key]
        } else {
          return [childNode.value !== undefined && key]
            .filter(Boolean)
            .concat(...this.keys(childNode).map(suffix => key + suffix))
        }
      }),
    )
  }
}
